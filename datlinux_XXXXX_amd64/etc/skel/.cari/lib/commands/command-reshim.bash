# -*- sh -*-

# shellcheck source=lib/commands/reshim.bash
#. "$(dirname "$CARI_CMD_FILE")/reshim.bash"

reshim_command() {
  local plugin_name=$1
  local full_version=$2

  if [ -z "$plugin_name" ]; then
    local plugins_path
    plugins_path=$(get_plugin_path)

    if ls "$plugins_path" &>/dev/null; then
      for plugin_path in "$plugins_path"/*; do
        plugin_name=$(basename "$plugin_path")
        reshim_command "$plugin_name"
      done
    fi
    return 0
  fi

  check_if_plugin_exists "$plugin_name"
  ensure_shims_dir

  if [ "$full_version" != "" ]; then
    # generate for the whole package version
    cari_run_hook "pre_cari_reshim_$plugin_name" "$full_version"
    generate_shims_for_version "$plugin_name" "$full_version"
    cari_run_hook "post_cari_reshim_$plugin_name" "$full_version"
  else
    # generate for all versions of the package
    local plugin_installs_path
    plugin_installs_path="$(cari_data_dir)/installs/${plugin_name}"

    for install in "${plugin_installs_path}"/*/; do
      local full_version_name
      full_version_name=$(basename "$install" | sed 's/ref\-/ref\:/')
      cari_run_hook "pre_cari_reshim_$plugin_name" "$full_version_name"
      generate_shims_for_version "$plugin_name" "$full_version_name"
      remove_obsolete_shims "$plugin_name" "$full_version_name"
      cari_run_hook "post_cari_reshim_$plugin_name" "$full_version_name"
    done
  fi

}

ensure_shims_dir() {
  # Create shims dir if doesn't exist
  if [ ! -d "$(cari_data_dir)/shims" ]; then
    mkdir "$(cari_data_dir)/shims"
  fi
}

write_shim_script() {
  local plugin_name=$1
  local version=$2
  local executable_path=$3
  
  if ! is_executable "$executable_path"; then
    return 0
  fi

  local executable_name
  executable_name=$(basename "$executable_path")

  local shim_path
  shim_path="$(cari_data_dir)/shims/$executable_name"

  if [ -f "$shim_path" ]; then
    if ! grep -x "# cari-plugin: ${plugin_name} ${version}" "$shim_path" >/dev/null; then
      sed -i.bak -e "s/exec /# cari-plugin: ${plugin_name} ${version}\\"$'\n''exec /' "$shim_path"
      rm "$shim_path".bak
    fi
  else
    cat <<EOF >"$shim_path"
#!/usr/bin/env bash
# cari-plugin: ${plugin_name} ${version}
exec $(cari_dir)/bin/cari exec "${executable_name}" "\$@"
EOF
  fi

  chmod +x "$shim_path"
}

generate_shim_for_executable() {
  local plugin_name=$1
  local executable=$2
  printf "generate_shim_for_executable plugin_name $plugin_name\\n"
  printf "generate_shim_for_executable executable $executable\\n"
  check_if_plugin_exists "$plugin_name"

  local version
  IFS=':' read -r -a version_info <<<"$full_version"
  if [ "${version_info[0]}" = "ref" ]; then
    version="${version_info[1]}"
  else
    version="${version_info[0]}"
  fi

  write_shim_script "$plugin_name" "$version" "$executable"
}

generate_shims_for_version() {
  local plugin_name=$1
  local full_version=$2
  local all_executable_paths
  IFS=$'\n' read -rd '' -a all_executable_paths <<<"$(plugin_executables "$plugin_name" "$full_version")"
  for executable_path in "${all_executable_paths[@]}"; do
      bname=$(basename "$executable_path")
      if [[ "$bname" == "$plugin_name" ]]; then
          write_shim_script "$plugin_name" "$full_version" "$executable_path"
      fi
  done
}

remove_obsolete_shims() {
  local plugin_name=$1
  local full_version=$2

  local shims
  shims=$(plugin_shims "$plugin_name" "$full_version" | xargs -IX basename X | sort)

  local exec_names
  exec_names=$(plugin_executables "$plugin_name" "$full_version" | xargs -IX basename X | sort)

  local obsolete_shims
  local formatted_shims
  local formatted_exec_names

  # comm only takes to files, so we write this data to temp files so we can
  # pass it to comm.
  formatted_shims=$(mktemp /tmp/cari-command-reshim-formatted-shims.XXXXXX)
  printf "%s\\n" "$shims" >"$formatted_shims"

  formatted_exec_names=$(mktemp /tmp/cari-command-reshim-formatted-exec-names.XXXXXX)
  printf "%s\\n" "$exec_names" >"$formatted_exec_names"

  obsolete_shims=$(comm -23 "$formatted_shims" "$formatted_exec_names")
  rm -f "$formatted_exec_names" "$formatted_shims"

  for shim_name in $obsolete_shims; do
    remove_shim_for_version "$plugin_name" "$full_version" "$shim_name"
  done
}

reshim_command "$@"
