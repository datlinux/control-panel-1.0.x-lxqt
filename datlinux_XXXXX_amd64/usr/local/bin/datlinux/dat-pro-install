#!/bin/bash

echo -e "\e[1;36m🧰️ DAT Linux PRO installer.\e[0;m"
echo -e "\e[0;36m(!! Admin/root password will be required for this install.) \e[0;m"

# shellcheck disable=SC1091
. /usr/local/bin/datlinux/dat-ping
check_internet || check_internet_err_msg

pro_dir="$HOME/.config/datlinux/pro"
pro_key="$pro_dir/pro.key"
file_dest=$HOME/.cari/tmp
file_gpg=""
file_gz=""
client_email=""
client_secret=""
curl_opts=(-fSL#)

read_key() {
    read -rp "Enter the Client Email: " v
    client_email="$v"
    read -rp "Enter the Client SECRET: " x
    client_secret="$x"
}

run() {
    if [[ -f "$pro_key" && $(grep "_email" "$pro_key") ]]; then
        client_email="$(grep "_email" "$pro_key" | cut -d"=" -f2)"
        read -rp "Use existing license information (for $client_email)? [Y|n]: " input
        if [[ $input == "Y" || $input == "y" || $input == "" ]]; then
            client_secret="$(grep "_secret" "$pro_key" | cut -d"=" -f2)"
            echo "Using client email: $client_email"
            client_secret_ending_in="$(echo "$client_secret" | cut -d"-" -f5)"
            echo "Using client secret (ending in): ..$client_secret_ending_in"
        else
            read_key
        fi
    else
        echo "No existing license found.."
        read_key
    fi
    fetch
}

fetch() {
    {
    export CURLOPT_SSLVERSION="CURL_SSLVERSION_TLSv1_2"
    export CURLOPT_SSL_CIPHER_LIST="AES256+EECDH:AES256+EDH"
    file_gpg="$file_dest/$client_email.tar.gz.gpg"
    rm -f "$file_gpg" > /dev/null
    url="https://datlinux.com/clients/$client_email.tar.gz.gpg"
    curl "${curl_opts[@]}" -o "$file_gpg" -C - "$url" >/dev/null 2>&1
    } || fail "No such license matched!" 
    process
}

process() {
    {
    gpg --pinentry-mode=loopback --passphrase "$client_secret" "$file_gpg" >/dev/null 2>&1
    file_gz="$file_dest/$client_email.tar.gz";
    if [ ! -f "$file_gz" ]; then
        fail "Could not process the encrypted software package!"
    fi
    sudo mkdir -p /opt/datlinux >/dev/null 2>&1;
    sudo tar -xzf "$file_gz" -C "/opt/datlinux/" --strip-components=1 >/dev/null 2>&1
    } || fail "Could not process the gzipped software package!"
    finalize
}

finalize() {
    if [ -f "$pro_key" ]; then
        rm "$pro_key"
    else
        mkdir -p "$pro_dir"
    fi
    touch "$pro_key"
    echo "client_email=$client_email
client_secret=$client_secret" | sudo tee -a "$pro_key" &>/dev/null
    /opt/datlinux/pro/api init >/dev/null 2>&1
    cleanup
    echo -e "
\e[0;36mDAT Linux PRO install successful!\e[0;m

Please close and re-open the DAT Linux Control Panel..\e[1;m"
    sleep 5
    exit 0
}

fail() {
    echo "
FAIL! $1
"
    cleanup
    sleep 10
    exit 1
}

cleanup() {
    rm  -f "$file_gz" >/dev/null 2>&1
    rm  -f "$file_gpg" >/dev/null 2>&1
}

if [[ "$1" != "upgrade" && -f /opt/datlinux/pro/uninstall ]]; then
    echo "
DAT Linux PRO is already installed..
"
    sleep 5 && exit 1
fi

run
