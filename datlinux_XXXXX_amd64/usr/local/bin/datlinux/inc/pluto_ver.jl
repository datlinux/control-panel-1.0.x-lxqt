using Pkg

function pkgver(pk::AbstractString)
    for v in values(Pkg.dependencies())
        if v.name == "Pluto"
            println(v.version)
            break
        end
    end
end

pkgver("Pluto")

