import sys
from tkinter import Tk 
from tkinter.filedialog import askdirectory
from pathlib import Path

dir = Path.home()
try:
    dir = str(sys.argv[1])
except:
    print('No arg, using home..')

Tk().withdraw()
filename = askdirectory(initialdir=dir)
print(filename)
