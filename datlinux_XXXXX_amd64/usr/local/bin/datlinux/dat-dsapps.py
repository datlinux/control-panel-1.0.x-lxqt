#!/usr/bin/python3

import os
import re
import socket
import sys
import time
import locale
import threading
import textwrap
import webbrowser
import configparser
import tkinter as tk
import tkinter.font as font
import tkinter.ttk as ttk
from datetime import datetime
from functools import partial
from pathlib import Path
from PIL import Image, ImageTk
from idlelib.tooltip import Hovertip
from ttkthemes import ThemedTk
from threading import Thread
from threading import Event

LOCK_PROC = 'dsappspy'
REPO = 'https://gitlab.com/datlinux/control-panel-1.0.x-lxqt'
CTRLPNLVER='1.0.171-lxqt'
APPS_PATH = '/usr/share/applications/datlinux'

def get_lock(process_name):
    get_lock._lock_socket = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    try:
        get_lock._lock_socket.bind('\0' + process_name)
        print('I got the lock')
    except socket.error:
        print('lock exists')
        sys.exit()

get_lock(LOCK_PROC)

def system_lang():
    default, _ = locale.getdefaultlocale()
    lan = default.split("_")[0]
    return lan

lang = system_lang()

if lang != 'en':
    tr = {}
    tr['af_ab'] = "Oor"
    tr['sq_ab'] = "Rreth"
    tr['am_ab'] = "ስለ"
    tr['ar_ab'] = "حول"
    tr['hy_ab'] = "Մասին"
    tr['az_ab'] = "Haqqında"
    tr['eu_ab'] = "Buruz"
    tr['be_ab'] = "Аб"
    tr['bn_ab'] = "সম্পর্কিত"
    tr['bs_ab'] = "O"
    tr['bg_ab'] = "относно"
    tr['ca_ab'] = "Sobre"
    tr['ceb_ab'] = "Mahitungod sa"
    tr['ny_ab'] = "Za"
    tr['zh_ab'] = "關於"
    tr['co_ab'] = "À propositu"
    tr['hr_ab'] = "Oko"
    tr['cs_ab'] = "O"
    tr['da_ab'] = "Om"
    tr['nl_ab'] = "Over"
    tr['en_ab'] = "About"
    tr['eo_ab'] = "Pri"
    tr['et_ab'] = "Umbes"
    tr['tl_ab'] = "Tungkol sa"
    tr['fi_ab'] = "Noin"
    tr['fr_ab'] = "À propos de"
    tr['fy_ab'] = "Oer"
    tr['gl_ab'] = "Sobre"
    tr['ka_ab'] = "შესახებ"
    tr['de_ab'] = "Um"
    tr['el_ab'] = "Σχετικά με"
    tr['gu_ab'] = "વિશે"
    tr['ht_ab'] = "Konsènan"
    tr['ha_ab'] = "Game da"
    tr['haw_ab'] = "E pili ana"
    tr['iw_ab'] = "על אודות"
    tr['he_ab'] = "על אודות"
    tr['hi_ab'] = "के बारे में"
    tr['hmn_ab'] = "Txog"
    tr['hu_ab'] = "Ról ről"
    tr['is_ab'] = "Um"
    tr['ig_ab'] = "Ihe gbasara"
    tr['id_ab'] = "Tentang"
    tr['ga_ab'] = "Faoi"
    tr['it_ab'] = "Di"
    tr['ja_ab'] = "約"
    tr['jw_ab'] = "Babagan"
    tr['kn_ab'] = "ಬಗ್ಗೆ"
    tr['kk_ab'] = "туралы"
    tr['km_ab'] = "អំពី"
    tr['ko_ab'] = "에 대한"
    tr['ku_ab'] = "Ji dor"
    tr['ky_ab'] = "Жөнүндө"
    tr['lo_ab'] = "ກ່ຽວກັບ"
    tr['la_ab'] = "De"
    tr['lv_ab'] = "Par"
    tr['lt_ab'] = "Apie"
    tr['lb_ab'] = "Iwwer"
    tr['mk_ab'] = "За"
    tr['mg_ab'] = "About"
    tr['ms_ab'] = "Tentang"
    tr['ml_ab'] = "കുറിച്ച്"
    tr['mt_ab'] = "Dwar"
    tr['mi_ab'] = "Mō"
    tr['mr_ab'] = "बद्दल"
    tr['mn_ab'] = "тухай"
    tr['my_ab'] = "အကြောင်း"
    tr['ne_ab'] = "बारे"
    tr['no_ab'] = "Om"
    tr['or_ab'] = "ବିଷୟରେ"
    tr['ps_ab'] = "په اړه"
    tr['fa_ab'] = "در باره"
    tr['pl_ab'] = "O"
    tr['pt_ab'] = "Sobre"
    tr['pa_ab'] = "ਬਾਰੇ"
    tr['ro_ab'] = "Despre"
    tr['ru_ab'] = "О"
    tr['sm_ab'] = "E uiga i"
    tr['gd_ab'] = "Mu dheidhinn"
    tr['sr_ab'] = "О томе"
    tr['st_ab'] = "About"
    tr['sn_ab'] = "About"
    tr['sd_ab'] = "بابت"
    tr['si_ab'] = "ගැන"
    tr['sk_ab'] = "O"
    tr['sl_ab'] = "O tem"
    tr['so_ab'] = "Ku saabsan"
    tr['es_ab'] = "Sobre"
    tr['su_ab'] = "Ngeunaan"
    tr['sw_ab'] = "Kuhusu"
    tr['sv_ab'] = "Handla om"
    tr['tg_ab'] = "Дар бораи"
    tr['ta_ab'] = "பற்றி"
    tr['te_ab'] = "గురించి"
    tr['th_ab'] = "เกี่ยวกับ"
    tr['tr_ab'] = "Hakkında"
    tr['uk_ab'] = "про"
    tr['ur_ab'] = "کے بارے میں"
    tr['ug_ab'] = "ھەققىدە"
    tr['uz_ab'] = "Haqida"
    tr['vi_ab'] = "Về"
    tr['cy_ab'] = "Ynghylch"
    tr['xh_ab'] = "Malunga"
    tr['yi_ab'] = "וועגן"
    tr['yo_ab'] = "Nipa"
    tr['zu_ab'] = "Mayelana"
    tr['af_di'] = "Verspreiding"
    tr['sq_di'] = "Shpërndarja"
    tr['am_di'] = "ስርጭት"
    tr['ar_di'] = "توزيع"
    tr['hy_di'] = "Բաշխում"
    tr['az_di'] = "Paylanma"
    tr['eu_di'] = "Banaketa"
    tr['be_di'] = "Размеркаванне"
    tr['bn_di'] = "বিতরণ"
    tr['bs_di'] = "Distribucija"
    tr['bg_di'] = "Разпределение"
    tr['ca_di'] = "Distribució"
    tr['ceb_di'] = "Pag-apod-apod"
    tr['ny_di'] = "Kugawa"
    tr['zh_di'] = "分配"
    tr['co_di'] = "Distribuzione"
    tr['hr_di'] = "Distribucija"
    tr['cs_di'] = "Rozdělení"
    tr['da_di'] = "Fordeling"
    tr['nl_di'] = "Verdeling"
    tr['en_di'] = "Distribution"
    tr['eo_di'] = "Distribuo"
    tr['et_di'] = "Levitamine"
    tr['tl_di'] = "Pamamahagi"
    tr['fi_di'] = "Jakelu"
    tr['fr_di'] = "Distribution"
    tr['fy_di'] = "Distribúsje"
    tr['gl_di'] = "Distribución"
    tr['ka_di'] = "დისტრიბუცია"
    tr['de_di'] = "Verteilung"
    tr['el_di'] = "Διανομή"
    tr['gu_di'] = "વિતરણ"
    tr['ht_di'] = "Distribisyon"
    tr['ha_di'] = "Rarrabawa"
    tr['haw_di'] = "Hoʻolaha"
    tr['iw_di'] = "הפצה"
    tr['he_di'] = "הפצה"
    tr['hi_di'] = "वितरण"
    tr['hmn_di'] = "Kev faib tawm"
    tr['hu_di'] = "terjesztés"
    tr['is_di'] = "Dreifing"
    tr['ig_di'] = "Nkesa"
    tr['id_di'] = "Distribusi"
    tr['ga_di'] = "Dáileadh"
    tr['it_di'] = "Distribuzione"
    tr['ja_di'] = "分布"
    tr['jw_di'] = "Distribusi"
    tr['kn_di'] = "ವಿತರಣೆ"
    tr['kk_di'] = "Тарату"
    tr['km_di'] = "ការចែកចាយ"
    tr['ko_di'] = "분포"
    tr['ku_di'] = "Belavkirinî"
    tr['ky_di'] = "Бөлүштүрүү"
    tr['lo_di'] = "ການແຜ່ກະຈາຍ"
    tr['la_di'] = "Distributio"
    tr['lv_di'] = "Izplatīšana"
    tr['lt_di'] = "Paskirstymas"
    tr['lb_di'] = "Verdeelung"
    tr['mk_di'] = "Дистрибуција"
    tr['mg_di'] = "fizarana"
    tr['ms_di'] = "Pengagihan"
    tr['ml_di'] = "വിതരണ"
    tr['mt_di'] = "Distribuzzjoni"
    tr['mi_di'] = "Tohanga"
    tr['mr_di'] = "वितरण"
    tr['mn_di'] = "Хуваарилалт"
    tr['my_di'] = "ဖြန့်ဝေခြင်း။"
    tr['ne_di'] = "वितरण"
    tr['no_di'] = "Fordeling"
    tr['or_di'] = "ବଣ୍ଟନ"
    tr['ps_di'] = "ویش"
    tr['fa_di'] = "توزیع"
    tr['pl_di'] = "Dystrybucja"
    tr['pt_di'] = "Distribuição"
    tr['pa_di'] = "ਵੰਡ"
    tr['ro_di'] = "Distributie"
    tr['ru_di'] = "Распределение"
    tr['sm_di'] = "Fa'asoa"
    tr['gd_di'] = "Sgaoileadh"
    tr['sr_di'] = "Дистрибуција"
    tr['st_di'] = "Kabo"
    tr['sn_di'] = "Distribution"
    tr['sd_di'] = "ورهائڻ"
    tr['si_di'] = "බෙදා හැරීම"
    tr['sk_di'] = "Distribúcia"
    tr['sl_di'] = "Distribucija"
    tr['so_di'] = "Qaybinta"
    tr['es_di'] = "Distribución"
    tr['su_di'] = "Distribusi"
    tr['sw_di'] = "Usambazaji"
    tr['sv_di'] = "Distribution"
    tr['tg_di'] = "Тақсим"
    tr['ta_di'] = "விநியோகம்"
    tr['te_di'] = "పంపిణీ"
    tr['th_di'] = "การกระจาย"
    tr['tr_di'] = "Dağıtım"
    tr['uk_di'] = "Розподіл"
    tr['ur_di'] = "تقسیم"
    tr['ug_di'] = "تارقىتىش"
    tr['uz_di'] = "Tarqatish"
    tr['vi_di'] = "Phân bổ"
    tr['cy_di'] = "Dosbarthiad"
    tr['xh_di'] = "Ukusasazwa"
    tr['yi_di'] = "פאַרשפּרייטונג"
    tr['yo_di'] = "Pinpin"
    tr['zu_di'] = "Ukusabalalisa"
    tr['af_cp'] = "Beheer paneel"
    tr['sq_cp'] = "Paneli i kontrollit"
    tr['am_cp'] = "መቆጣጠሪያ ሰሌዳ"
    tr['ar_cp'] = "لوحة التحكم"
    tr['hy_cp'] = "Կառավարման վահանակ"
    tr['az_cp'] = "İdarə paneli"
    tr['eu_cp'] = "Kontrol panela"
    tr['be_cp'] = "Панэль кіравання"
    tr['bn_cp'] = "কন্ট্রোল প্যানেল"
    tr['bs_cp'] = "Kontrolna tabla"
    tr['bg_cp'] = "Контролен панел"
    tr['ca_cp'] = "Panell de control"
    tr['ceb_cp'] = "Control Panel"
    tr['ny_cp'] = "Gawo lowongolera"
    tr['zh_cp'] = "控制面板"
    tr['co_cp'] = "Panel di cuntrollu"
    tr['hr_cp'] = "Upravljačka ploča"
    tr['cs_cp'] = "Kontrolní panel"
    tr['da_cp'] = "Kontrolpanel"
    tr['nl_cp'] = "Controlepaneel"
    tr['en_cp'] = "Control Panel"
    tr['eo_cp'] = "Kontrola Panelo"
    tr['et_cp'] = "Kontrollpaneel"
    tr['tl_cp'] = "Control Panel"
    tr['fi_cp'] = "Ohjauspaneeli"
    tr['fr_cp'] = "Panneau de commande"
    tr['fy_cp'] = "Control Panel"
    tr['gl_cp'] = "Panel de control"
    tr['ka_cp'] = "Მართვის პანელი"
    tr['de_cp'] = "Schalttafel"
    tr['el_cp'] = "Πίνακας Ελέγχου"
    tr['gu_cp'] = "કંટ્રોલ પેનલ"
    tr['ht_cp'] = "Kontwòl Panel"
    tr['ha_cp'] = "Kwamitin Kulawa"
    tr['haw_cp'] = "Panel Manao"
    tr['iw_cp'] = "לוח בקרה"
    tr['he_cp'] = "לוח בקרה"
    tr['hi_cp'] = "कंट्रोल पैनल"
    tr['hmn_cp'] = "Tswj Vaj Huam Sib Luag"
    tr['hu_cp'] = "Vezérlőpult"
    tr['is_cp'] = "Stjórnborð"
    tr['ig_cp'] = "Ogwe njikwa"
    tr['id_cp'] = "Panel kendali"
    tr['ga_cp'] = "Painéal rialú"
    tr['it_cp'] = "Pannello di controllo"
    tr['ja_cp'] = "コントロールパネル"
    tr['jw_cp'] = "Panel Kontrol"
    tr['kn_cp'] = "ನಿಯಂತ್ರಣಫಲಕ"
    tr['kk_cp'] = "Басқару панелі"
    tr['km_cp'] = "ផ្ទាំងបញ្ជា"
    tr['ko_cp'] = "제어판"
    tr['ku_cp'] = "Panela Kontrolê"
    tr['ky_cp'] = "Башкаруу панели"
    tr['lo_cp'] = "ກະດານຄວບຄຸມ"
    tr['la_cp'] = "Imperium Panel"
    tr['lv_cp'] = "Vadības panelis"
    tr['lt_cp'] = "Kontrolės skydelis"
    tr['lb_cp'] = "Kontroll Panel"
    tr['mk_cp'] = "Контролен панел"
    tr['mg_cp'] = "Takelaka fikirakirana"
    tr['ms_cp'] = "Panel kawalan"
    tr['ml_cp'] = "നിയന്ത്രണ പാനൽ"
    tr['mt_cp'] = "Panel tal-Kontroll"
    tr['mi_cp'] = "Paewhiri Mana"
    tr['mr_cp'] = "नियंत्रण पॅनेल"
    tr['mn_cp'] = "Хяналтын самбар"
    tr['my_cp'] = "ထိန်းချုပ်ရာနေရာ"
    tr['ne_cp'] = "नियन्त्रण कक्ष"
    tr['no_cp'] = "Kontrollpanel"
    tr['or_cp'] = "କଣ୍ଟ୍ରୋଲ୍ ପ୍ୟାନେଲ୍ |"
    tr['ps_cp'] = "د تضمین اداره"
    tr['fa_cp'] = "صفحه کنترل"
    tr['pl_cp'] = "Panel sterowania"
    tr['pt_cp'] = "Painel de controle"
    tr['pa_cp'] = "ਕਨ੍ਟ੍ਰੋਲ ਪੈਨਲ"
    tr['ro_cp'] = "Panou de control"
    tr['ru_cp'] = "Панель управления"
    tr['sm_cp'] = "Vaega Pule"
    tr['gd_cp'] = "Pannal Smachd"
    tr['sr_cp'] = "Контролна табла"
    tr['st_cp'] = "Lekhotla la taolo"
    tr['sn_cp'] = "Control Panel"
    tr['sd_cp'] = "ڪنٽرول پينل"
    tr['si_cp'] = "පාලන පුවරුව"
    tr['sk_cp'] = "Ovládací panel"
    tr['sl_cp'] = "Nadzorna plošča"
    tr['so_cp'] = "Guddiga xakamaynta"
    tr['es_cp'] = "Panel de control"
    tr['su_cp'] = "Panél kontrol"
    tr['sw_cp'] = "Jopo kudhibiti"
    tr['sv_cp'] = "Kontrollpanel"
    tr['tg_cp'] = "Сафҳаи идоракунӣ"
    tr['ta_cp'] = "கண்ட்ரோல் பேனல்"
    tr['te_cp'] = "నియంత్రణ ప్యానెల్"
    tr['th_cp'] = "แผงควบคุม"
    tr['tr_cp'] = "Kontrol Paneli"
    tr['uk_cp'] = "Панель управління"
    tr['ur_cp'] = "کنٹرول پینل"
    tr['ug_cp'] = "كونترول تاختىسى"
    tr['uz_cp'] = "Boshqaruv paneli"
    tr['vi_cp'] = "Bảng điều khiển"
    tr['cy_cp'] = "Panel Rheoli"
    tr['xh_cp'] = "Iphaneli yokulawula"
    tr['yi_cp'] = "קאָנטראָל פּאַנעל"
    tr['yo_cp'] = "Ibi iwaju alabujuto"
    tr['zu_cp'] = "Iphaneli yokulawula"
    tr['af_ds_1'] = "Datawetenskap"
    tr['sq_ds_1'] = "Shkenca e të Dhënave"
    tr['am_ds_1'] = "የውሂብ ሳይንስ"
    tr['ar_ds_1'] = "علم البيانات"
    tr['hy_ds_1'] = "Տվյալների գիտություն"
    tr['az_ds_1'] = "Məlumat Elmi"
    tr['eu_ds_1'] = "Datuen Zientzia"
    tr['be_ds_1'] = "Навука аб дадзеных"
    tr['bn_ds_1'] = "ডেটা সায়েন্স"
    tr['bs_ds_1'] = "Data Science"
    tr['bg_ds_1'] = "Наука за данни"
    tr['ca_ds_1'] = "Ciència de dades"
    tr['ceb_ds_1'] = "Data Science"
    tr['ny_ds_1'] = "Sayansi ya Data"
    tr['zh_ds_1'] = "數據科學"
    tr['co_ds_1'] = "Data Science"
    tr['hr_ds_1'] = "Znanost o podacima"
    tr['cs_ds_1'] = "Data Science"
    tr['da_ds_1'] = "Datavidenskab"
    tr['nl_ds_1'] = "Gegevenswetenschap"
    tr['en_ds_1'] = "Data Science"
    tr['eo_ds_1'] = "Datumscienco"
    tr['et_ds_1'] = "Andmeteadus"
    tr['tl_ds_1'] = "Agham ng Data"
    tr['fi_ds_1'] = "Tietotiede"
    tr['fr_ds_1'] = "Science des données"
    tr['fy_ds_1'] = "Data Science"
    tr['gl_ds_1'] = "Ciencia de datos"
    tr['ka_ds_1'] = "მონაცემთა მეცნიერება"
    tr['de_ds_1'] = "Datenwissenschaft"
    tr['el_ds_1'] = "Επιστημονικά δεδομένα"
    tr['gu_ds_1'] = "ડેટા સાયન્સ"
    tr['ht_ds_1'] = "Syans done"
    tr['ha_ds_1'] = "Kimiyyar Bayanai"
    tr['haw_ds_1'] = "ʻEpekema ʻIkepili"
    tr['iw_ds_1'] = "מדע נתונים"
    tr['he_ds_1'] = "מדע נתונים"
    tr['hi_ds_1'] = "डेटा विज्ञान"
    tr['hmn_ds_1'] = "Data Science"
    tr['hu_ds_1'] = "Adattudomány"
    tr['is_ds_1'] = "Gagnafræði"
    tr['ig_ds_1'] = "Sayensị data"
    tr['id_ds_1'] = "Ilmu Data"
    tr['ga_ds_1'] = "Eolaíocht Sonraí"
    tr['it_ds_1'] = "Scienza dei dati"
    tr['ja_ds_1'] = "データサイエンス"
    tr['jw_ds_1'] = "Ilmu Data"
    tr['kn_ds_1'] = "ಡೇಟಾ ಸೈನ್ಸ್"
    tr['kk_ds_1'] = "Деректер туралы ғылым"
    tr['km_ds_1'] = "វិទ្យាសាស្ត្រទិន្នន័យ"
    tr['ko_ds_1'] = "데이터 과학"
    tr['ku_ds_1'] = "Daneyên Zanistî"
    tr['ky_ds_1'] = "Data Science"
    tr['lo_ds_1'] = "ວິທະຍາສາດຂໍ້ມູນ"
    tr['la_ds_1'] = "Data Scientiae"
    tr['lv_ds_1'] = "Datu zinātne"
    tr['lt_ds_1'] = "Duomenų mokslas"
    tr['lb_ds_1'] = "Data Science"
    tr['mk_ds_1'] = "Наука за податоци"
    tr['mg_ds_1'] = "Data Science"
    tr['ms_ds_1'] = "Sains Data"
    tr['ml_ds_1'] = "ഡാറ്റ സയൻസ്"
    tr['mt_ds_1'] = "Xjenza tad-Data"
    tr['mi_ds_1'] = "Pūtaiao Raraunga"
    tr['mr_ds_1'] = "डेटा सायन्स"
    tr['mn_ds_1'] = "Өгөгдлийн шинжлэх ухаан"
    tr['my_ds_1'] = "ဒေတာသိပ္ပံ"
    tr['ne_ds_1'] = "डाटा विज्ञान"
    tr['no_ds_1'] = "Datavitenskap"
    tr['or_ds_1'] = "ଡାଟା ସାଇନ୍ସ |"
    tr['ps_ds_1'] = "د معلوماتو ساینس"
    tr['fa_ds_1'] = "علم داده"
    tr['pl_ds_1'] = "Nauka o danych"
    tr['pt_ds_1'] = "Ciência de dados"
    tr['pa_ds_1'] = "ਡਾਟਾ ਸਾਇੰਸ"
    tr['ro_ds_1'] = "Știința datelor"
    tr['ru_ds_1'] = "Наука о данных"
    tr['sm_ds_1'] = "Saienisi Fa'amatalaga"
    tr['gd_ds_1'] = "Saidheans Dàta"
    tr['sr_ds_1'] = "Дата Сциенце"
    tr['st_ds_1'] = "Saense ea Boitsebiso"
    tr['sn_ds_1'] = "Data Science"
    tr['sd_ds_1'] = "ڊيٽا سائنس"
    tr['si_ds_1'] = "දත්ත විද්යාව"
    tr['sk_ds_1'] = "Data Science"
    tr['sl_ds_1'] = "Podatkovna znanost"
    tr['so_ds_1'] = "Sayniska Xogta"
    tr['es_ds_1'] = "Ciencia de los datos"
    tr['su_ds_1'] = "Élmu Data"
    tr['sw_ds_1'] = "Sayansi ya Data"
    tr['sv_ds_1'] = "Datavetenskap"
    tr['tg_ds_1'] = "Илми маълумот"
    tr['ta_ds_1'] = "தரவு அறிவியல்"
    tr['te_ds_1'] = "డేటా సైన్స్"
    tr['th_ds_1'] = "วิทยาศาสตร์ข้อมูล"
    tr['tr_ds_1'] = "Veri Bilimi"
    tr['uk_ds_1'] = "Data Science"
    tr['ur_ds_1'] = "ڈیٹا سائنس"
    tr['ug_ds_1'] = "Data Science"
    tr['uz_ds_1'] = "Ma'lumotlar fan"
    tr['vi_ds_1'] = "Khoa học dữ liệu"
    tr['cy_ds_1'] = "Gwyddor Data"
    tr['xh_ds_1'] = "Inzululwazi yeDatha"
    tr['yi_ds_1'] = "דאַטאַ וויסנשאַפֿט"
    tr['yo_ds_1'] = "Data Imọ"
    tr['zu_ds_1'] = "Isayensi Yedatha"
    tr['af_ds_2'] = "Toepassings"
    tr['sq_ds_2'] = "Aplikacionet"
    tr['am_ds_2'] = "መተግበሪያዎች"
    tr['ar_ds_2'] = "تطبيقات"
    tr['hy_ds_2'] = "Հավելվածներ"
    tr['az_ds_2'] = "Proqramlar"
    tr['eu_ds_2'] = "Aplikazioak"
    tr['be_ds_2'] = "Праграмы"
    tr['bn_ds_2'] = "অ্যাপস"
    tr['bs_ds_2'] = "Apps"
    tr['bg_ds_2'] = "Приложения"
    tr['ca_ds_2'] = "Aplicacions"
    tr['ceb_ds_2'] = "Aplikasyon"
    tr['ny_ds_2'] = "Mapulogalamu"
    tr['zh_ds_2'] = "應用"
    tr['co_ds_2'] = "Apps"
    tr['hr_ds_2'] = "aplikacije"
    tr['cs_ds_2'] = "Aplikace"
    tr['da_ds_2'] = "Apps"
    tr['nl_ds_2'] = "Apps"
    tr['en_ds_2'] = "Apps"
    tr['eo_ds_2'] = "Aplikoj"
    tr['et_ds_2'] = "Rakendused"
    tr['tl_ds_2'] = "Mga app"
    tr['fi_ds_2'] = "Sovellukset"
    tr['fr_ds_2'] = "applications"
    tr['fy_ds_2'] = "Apps"
    tr['gl_ds_2'] = "Aplicacións"
    tr['ka_ds_2'] = "აპები"
    tr['de_ds_2'] = "Anwendungen"
    tr['el_ds_2'] = "Εφαρμογές"
    tr['gu_ds_2'] = "એપ્સ"
    tr['ht_ds_2'] = "Aplikasyon yo"
    tr['ha_ds_2'] = "Aikace-aikace"
    tr['haw_ds_2'] = "Apps"
    tr['iw_ds_2'] = "אפליקציות"
    tr['he_ds_2'] = "אפליקציות"
    tr['hi_ds_2'] = "ऐप्स"
    tr['hmn_ds_2'] = "Apps"
    tr['hu_ds_2'] = "Alkalmazások elemre"
    tr['is_ds_2'] = "Forrit"
    tr['ig_ds_2'] = "Ngwa"
    tr['id_ds_2'] = "Aplikasi"
    tr['ga_ds_2'] = "Feidhmchláir"
    tr['it_ds_2'] = "App"
    tr['ja_ds_2'] = "アプリ"
    tr['jw_ds_2'] = "Aplikasi"
    tr['kn_ds_2'] = "ಅಪ್ಲಿಕೇಶನ್ಗಳು"
    tr['kk_ds_2'] = "Қолданбалар"
    tr['km_ds_2'] = "កម្មវិធី"
    tr['ko_ds_2'] = "앱"
    tr['ku_ds_2'] = "Apps"
    tr['ky_ds_2'] = "Колдонмолор"
    tr['lo_ds_2'] = "ແອັບ"
    tr['la_ds_2'] = "Apps"
    tr['lv_ds_2'] = "Lietotnes"
    tr['lt_ds_2'] = "Programėlės"
    tr['lb_ds_2'] = "Apps"
    tr['mk_ds_2'] = "Апликации"
    tr['mg_ds_2'] = "app"
    tr['ms_ds_2'] = "Apl"
    tr['ml_ds_2'] = "ആപ്പുകൾ"
    tr['mt_ds_2'] = "Apps"
    tr['mi_ds_2'] = "Taupānga"
    tr['mr_ds_2'] = "अॅप्स"
    tr['mn_ds_2'] = "Програмууд"
    tr['my_ds_2'] = "အက်ပ်များ"
    tr['ne_ds_2'] = "एपहरू"
    tr['no_ds_2'] = "Apper"
    tr['or_ds_2'] = "ଅନୁପ୍ରୟୋଗଗୁଡିକ"
    tr['ps_ds_2'] = "ایپس"
    tr['fa_ds_2'] = "برنامه ها"
    tr['pl_ds_2'] = "Aplikacje"
    tr['pt_ds_2'] = "Aplicativos"
    tr['pa_ds_2'] = "ਐਪਸ"
    tr['ro_ds_2'] = "Aplicații"
    tr['ru_ds_2'] = "Программы"
    tr['sm_ds_2'] = "Apps"
    tr['gd_ds_2'] = "Apps"
    tr['sr_ds_2'] = "Аппс"
    tr['st_ds_2'] = "Lisebelisoa"
    tr['sn_ds_2'] = "Apps"
    tr['sd_ds_2'] = "ايپس"
    tr['si_ds_2'] = "යෙදුම්"
    tr['sk_ds_2'] = "Aplikácie"
    tr['sl_ds_2'] = "Aplikacije"
    tr['so_ds_2'] = "Apps"
    tr['es_ds_2'] = "aplicaciones"
    tr['su_ds_2'] = "Aplikasi"
    tr['sw_ds_2'] = "Programu"
    tr['sv_ds_2'] = "Appar"
    tr['tg_ds_2'] = "Барномаҳо"
    tr['ta_ds_2'] = "பயன்பாடுகள்"
    tr['te_ds_2'] = "యాప్‌లు"
    tr['th_ds_2'] = "แอพ"
    tr['tr_ds_2'] = "Uygulamalar"
    tr['uk_ds_2'] = "програми"
    tr['ur_ds_2'] = "ایپس"
    tr['ug_ds_2'] = "ئەپلەر"
    tr['uz_ds_2'] = "Ilovalar"
    tr['vi_ds_2'] = "Ứng dụng"
    tr['cy_ds_2'] = "Apiau"
    tr['xh_ds_2'] = "Usetyenziso"
    tr['yi_ds_2'] = "אַפּפּס"
    tr['yo_ds_2'] = "Awọn ohun elo"
    tr['zu_ds_2'] = "Izinhlelo zokusebenza"
    tr['af_ex'] = "Ekstras"
    tr['sq_ex'] = "Shtesa"
    tr['am_ex'] = "ተጨማሪዎች"
    tr['ar_ex'] = "إضافات"
    tr['hy_ex'] = "Հավելյալներ"
    tr['az_ex'] = "Əlavələr"
    tr['eu_ex'] = "Gehigarriak"
    tr['be_ex'] = "Дадаткова"
    tr['bn_ex'] = "অতিরিক্ত"
    tr['bs_ex'] = "Extras"
    tr['bg_ex'] = "Екстри"
    tr['ca_ex'] = "Extres"
    tr['ceb_ex'] = "Mga ekstra"
    tr['ny_ex'] = "Zowonjezera"
    tr['zh_ex'] = "附加功能"
    tr['co_ex'] = "Extras"
    tr['hr_ex'] = "Dodaci"
    tr['cs_ex'] = "Extra"
    tr['da_ex'] = "Ekstra"
    tr['nl_ex'] = "Extra's"
    tr['en_ex'] = "Extras"
    tr['eo_ex'] = "Kromaĵoj"
    tr['et_ex'] = "Lisad"
    tr['tl_ex'] = "Mga extra"
    tr['fi_ex'] = "Ekstrat"
    tr['fr_ex'] = "Suppléments"
    tr['fy_ex'] = "Extras"
    tr['gl_ex'] = "Extras"
    tr['ka_ex'] = "დამატებები"
    tr['de_ex'] = "Extras"
    tr['el_ex'] = "Πρόσθετα"
    tr['gu_ex'] = "એક્સ્ટ્રાઝ"
    tr['ht_ex'] = "Siplemantè"
    tr['ha_ex'] = "Kari"
    tr['haw_ex'] = "Nā mea hou aku"
    tr['iw_ex'] = "תוספות"
    tr['he_ex'] = "תוספות"
    tr['hi_ex'] = "अतिरिक्त"
    tr['hmn_ex'] = "Ntxiv"
    tr['hu_ex'] = "Extrák"
    tr['is_ex'] = "Aukahlutir"
    tr['ig_ex'] = "Mgbakwunye"
    tr['id_ex'] = "Ekstra"
    tr['ga_ex'] = "Breiseáin"
    tr['it_ex'] = "Extra"
    tr['ja_ex'] = "エクストラ"
    tr['jw_ex'] = "Ekstra"
    tr['kn_ex'] = "ಎಕ್ಸ್ಟ್ರಾಗಳು"
    tr['kk_ex'] = "Қосымшалар"
    tr['km_ex'] = "បន្ថែម"
    tr['ko_ex'] = "엑스트라"
    tr['ku_ex'] = "Extras"
    tr['ky_ex'] = "Кошумчалар"
    tr['lo_ex'] = "ພິເສດ"
    tr['la_ex'] = "Extras"
    tr['lv_ex'] = "Ekstras"
    tr['lt_ex'] = "Priedai"
    tr['lb_ex'] = "Extras"
    tr['mk_ex'] = "Додатоци"
    tr['mg_ex'] = "Fanampiny"
    tr['ms_ex'] = "Tambahan"
    tr['ml_ex'] = "എക്സ്ട്രാകൾ"
    tr['mt_ex'] = "Extras"
    tr['mi_ex'] = "Tāpiritanga"
    tr['mr_ex'] = "अवांतर"
    tr['mn_ex'] = "Нэмэлт"
    tr['my_ex'] = "အပိုတွေ"
    tr['ne_ex'] = "अतिरिक्त"
    tr['no_ex'] = "Ekstrautstyr"
    tr['or_ex'] = "ଅତିରିକ୍ତ"
    tr['ps_ex'] = "اضافي"
    tr['fa_ex'] = "موارد اضافی"
    tr['pl_ex'] = "Dodatki"
    tr['pt_ex'] = "Extras"
    tr['pa_ex'] = "ਵਾਧੂ"
    tr['ro_ex'] = "In plus"
    tr['ru_ex'] = "Дополнительно"
    tr['sm_ex'] = "Fa'aopoopo"
    tr['gd_ex'] = "Extras"
    tr['sr_ex'] = "Ектрас"
    tr['st_ex'] = "Keketso"
    tr['sn_ex'] = "Extras"
    tr['sd_ex'] = "اضافي"
    tr['si_ex'] = "අමතර"
    tr['sk_ex'] = "Extra"
    tr['sl_ex'] = "Dodatki"
    tr['so_ex'] = "Dheeraad ah"
    tr['es_ex'] = "Extras"
    tr['su_ex'] = "Tambahan"
    tr['sw_ex'] = "Ziada"
    tr['sv_ex'] = "Extrafunktioner"
    tr['tg_ex'] = "Иловаҳо"
    tr['ta_ex'] = "கூடுதல்"
    tr['te_ex'] = "ఎక్స్‌ట్రాలు"
    tr['th_ex'] = "ความพิเศษ"
    tr['tr_ex'] = "Ekstralar"
    tr['uk_ex'] = "Додатково"
    tr['ur_ex'] = "اضافی"
    tr['ug_ex'] = "قوشۇمچە"
    tr['uz_ex'] = "Qo'shimchalar"
    tr['vi_ex'] = "Bổ sung"
    tr['cy_ex'] = "Ychwanegiadau"
    tr['xh_ex'] = "Okongeziweyo"
    tr['yi_ex'] = "עקסטראַס"
    tr['yo_ex'] = "Awọn afikun"
    tr['zu_ex'] = "Okungeziwe"
    tr['af_ad'] = "admin"
    tr['sq_ad'] = "Admin"
    tr['am_ad'] = "አስተዳዳሪ"
    tr['ar_ad'] = "مسؤل"
    tr['hy_ad'] = "Ադմին"
    tr['az_ad'] = "Admin"
    tr['eu_ad'] = "Admin"
    tr['be_ad'] = "Адмін"
    tr['bn_ad'] = "অ্যাডমিন"
    tr['bs_ad'] = "Admin"
    tr['bg_ad'] = "Админ"
    tr['ca_ad'] = "Admin"
    tr['ceb_ad'] = "Admin"
    tr['ny_ad'] = "Admin"
    tr['zh_ad'] = "行政"
    tr['co_ad'] = "Admin"
    tr['hr_ad'] = "Administrator"
    tr['cs_ad'] = "Admin"
    tr['da_ad'] = "Admin"
    tr['nl_ad'] = "beheerder"
    tr['en_ad'] = "Admin"
    tr['eo_ad'] = "Admin"
    tr['et_ad'] = "Admin"
    tr['tl_ad'] = "Admin"
    tr['fi_ad'] = "Admin"
    tr['fr_ad'] = "Administrateur"
    tr['fy_ad'] = "Admin"
    tr['gl_ad'] = "Admin"
    tr['ka_ad'] = "ადმინ"
    tr['de_ad'] = "Administrator"
    tr['el_ad'] = "διαχειριστής"
    tr['gu_ad'] = "એડમિન"
    tr['ht_ad'] = "Admin"
    tr['ha_ad'] = "Admin"
    tr['haw_ad'] = "Admin"
    tr['iw_ad'] = "מנהל מערכת"
    tr['he_ad'] = "מנהל מערכת"
    tr['hi_ad'] = "व्यवस्थापक"
    tr['hmn_ad'] = "Admin"
    tr['hu_ad'] = "Admin"
    tr['is_ad'] = "Admin"
    tr['ig_ad'] = "Admin"
    tr['id_ad'] = "Admin"
    tr['ga_ad'] = "Riarachán"
    tr['it_ad'] = "amministratore"
    tr['ja_ad'] = "管理者"
    tr['jw_ad'] = "Admin"
    tr['kn_ad'] = "ನಿರ್ವಾಹಕ"
    tr['kk_ad'] = "Админ"
    tr['km_ad'] = "អ្នកគ្រប់គ្រង"
    tr['ko_ad'] = "관리자"
    tr['ku_ad'] = "Admin"
    tr['ky_ad'] = "Админ"
    tr['lo_ad'] = "ບໍລິຫານ"
    tr['la_ad'] = "Admin"
    tr['lv_ad'] = "Administrators"
    tr['lt_ad'] = "Admin"
    tr['lb_ad'] = "Admin"
    tr['mk_ad'] = "Админ"
    tr['mg_ad'] = "Admin"
    tr['ms_ad'] = "Admin"
    tr['ml_ad'] = "അഡ്മിൻ"
    tr['mt_ad'] = "Amministratur"
    tr['mi_ad'] = "Kaiwhakahaere"
    tr['mr_ad'] = "अॅडमिन"
    tr['mn_ad'] = "админ"
    tr['my_ad'] = "အက်မင်"
    tr['ne_ad'] = "व्यवस्थापक"
    tr['no_ad'] = "Admin"
    tr['or_ad'] = "ଆଡମିନି"
    tr['ps_ad'] = "اډمین"
    tr['fa_ad'] = "مدیر"
    tr['pl_ad'] = "Admin"
    tr['pt_ad'] = "Administrador"
    tr['pa_ad'] = "ਐਡਮਿਨ"
    tr['ro_ad'] = "Admin"
    tr['ru_ad'] = "Администратор"
    tr['sm_ad'] = "Pule"
    tr['gd_ad'] = "Rianachd"
    tr['sr_ad'] = "Админ"
    tr['st_ad'] = "Admin"
    tr['sn_ad'] = "Admin"
    tr['sd_ad'] = "ايڊمن"
    tr['si_ad'] = "පරිපාලක"
    tr['sk_ad'] = "Admin"
    tr['sl_ad'] = "skrbnik"
    tr['so_ad'] = "Admin"
    tr['es_ad'] = "Administración"
    tr['su_ad'] = "Admin"
    tr['sw_ad'] = "Msimamizi"
    tr['sv_ad'] = "Administration"
    tr['tg_ad'] = "Админ"
    tr['ta_ad'] = "நிர்வாகம்"
    tr['te_ad'] = "అడ్మిన్"
    tr['th_ad'] = "แอดมิน"
    tr['tr_ad'] = "yönetici"
    tr['uk_ad'] = "адмін"
    tr['ur_ad'] = "ایڈمن"
    tr['ug_ad'] = "باشقۇرغۇچى"
    tr['uz_ad'] = "Admin"
    tr['vi_ad'] = "Quản trị viên"
    tr['cy_ad'] = "Gweinyddol"
    tr['xh_ad'] = "Admin"
    tr['yi_ad'] = "אַדמין"
    tr['yo_ad'] = "Abojuto"
    tr['zu_ad'] = "Admin"
    tr['af_he'] = "Help"
    tr['sq_he'] = "Ndihmë"
    tr['am_he'] = "እገዛ"
    tr['ar_he'] = "مساعدة"
    tr['hy_he'] = "Օգնություն"
    tr['az_he'] = "Kömək edin"
    tr['eu_he'] = "Laguntza"
    tr['be_he'] = "Даведка"
    tr['bn_he'] = "সাহায্য"
    tr['bs_he'] = "Upomoć"
    tr['bg_he'] = "Помогне"
    tr['ca_he'] = "Ajuda"
    tr['ceb_he'] = "Tabang"
    tr['ny_he'] = "Thandizeni"
    tr['zh_he'] = "幫助"
    tr['co_he'] = "Aiutu"
    tr['hr_he'] = "Pomozite"
    tr['cs_he'] = "Pomoc"
    tr['da_he'] = "Hjælp"
    tr['nl_he'] = "Helpen"
    tr['en_he'] = "Help"
    tr['eo_he'] = "Helpu"
    tr['et_he'] = "Abi"
    tr['tl_he'] = "Tulong"
    tr['fi_he'] = "auta"
    tr['fr_he'] = "Aider"
    tr['fy_he'] = "Help"
    tr['gl_he'] = "Axuda"
    tr['ka_he'] = "დახმარება"
    tr['de_he'] = "Hilfe"
    tr['el_he'] = "Βοήθεια"
    tr['gu_he'] = "મદદ"
    tr['ht_he'] = "Ede"
    tr['ha_he'] = "Taimako"
    tr['haw_he'] = "Kokua"
    tr['iw_he'] = "עֶזרָה"
    tr['he_he'] = "עֶזרָה"
    tr['hi_he'] = "मदद करना"
    tr['hmn_he'] = "Pab"
    tr['hu_he'] = "Segítség"
    tr['is_he'] = "Hjálp"
    tr['ig_he'] = "Enyemaka"
    tr['id_he'] = "Membantu"
    tr['ga_he'] = "Cabhrú"
    tr['it_he'] = "Aiuto"
    tr['ja_he'] = "ヘルプ"
    tr['jw_he'] = "Pitulung"
    tr['kn_he'] = "ಸಹಾಯ"
    tr['kk_he'] = "Көмектесіңдер"
    tr['km_he'] = "ជំនួយ"
    tr['ko_he'] = "돕다"
    tr['ku_he'] = "Alîkarî"
    tr['ky_he'] = "Жардам"
    tr['lo_he'] = "ຊ່ວຍເຫຼືອ"
    tr['la_he'] = "Auxilium"
    tr['lv_he'] = "Palīdzība"
    tr['lt_he'] = "Pagalba"
    tr['lb_he'] = "Hëllef"
    tr['mk_he'] = "Помош"
    tr['mg_he'] = "Vonjeo"
    tr['ms_he'] = "Tolong"
    tr['ml_he'] = "സഹായം"
    tr['mt_he'] = "Għajnuna"
    tr['mi_he'] = "Awhina"
    tr['mr_he'] = "मदत करा"
    tr['mn_he'] = "Туслаач"
    tr['my_he'] = "ကူညီပါ"
    tr['ne_he'] = "मद्दत गर्नुहोस्"
    tr['no_he'] = "Hjelp"
    tr['or_he'] = "ସାହାଯ୍ୟ"
    tr['ps_he'] = "مرسته"
    tr['fa_he'] = "کمک"
    tr['pl_he'] = "Pomoc"
    tr['pt_he'] = "Ajuda"
    tr['pa_he'] = "ਮਦਦ ਕਰੋ"
    tr['ro_he'] = "Ajutor"
    tr['ru_he'] = "Помощь"
    tr['sm_he'] = "Fesoasoani"
    tr['gd_he'] = "Cuideachadh"
    tr['sr_he'] = "Помоћ"
    tr['st_he'] = "Thusa"
    tr['sn_he'] = "Help"
    tr['sd_he'] = "مدد"
    tr['si_he'] = "උදව්"
    tr['sk_he'] = "Pomoc"
    tr['sl_he'] = "pomoč"
    tr['so_he'] = "I caawi"
    tr['es_he'] = "Ayuda"
    tr['su_he'] = "Tulung"
    tr['sw_he'] = "Msaada"
    tr['sv_he'] = "Hjälp"
    tr['tg_he'] = "Кумак"
    tr['ta_he'] = "உதவி"
    tr['te_he'] = "సహాయం"
    tr['th_he'] = "ช่วย"
    tr['tr_he'] = "Yardım"
    tr['uk_he'] = "Довідка"
    tr['ur_he'] = "مدد"
    tr['ug_he'] = "ياردەم"
    tr['uz_he'] = "Yordam bering"
    tr['vi_he'] = "Cứu giúp"
    tr['cy_he'] = "Help"
    tr['xh_he'] = "Nceda"
    tr['yi_he'] = "הילף"
    tr['yo_he'] = "Egba Mi O"
    tr['zu_he'] = "Usizo"
    tr['af_tl'] = "Gereedskap"
    tr['sq_tl'] = "Mjetet"
    tr['am_tl'] = "መሳሪያዎች"
    tr['ar_tl'] = "أدوات"
    tr['hy_tl'] = "Գործիքներ"
    tr['az_tl'] = "Alətlər"
    tr['eu_tl'] = "Tresnak"
    tr['be_tl'] = "інструменты"
    tr['bn_tl'] = "টুলস"
    tr['bs_tl'] = "Alati"
    tr['bg_tl'] = "Инструменти"
    tr['ca_tl'] = "Eines"
    tr['ceb_tl'] = "Mga galamiton"
    tr['ny_tl'] = "Zida"
    tr['zh-cn_tl'] = "工具"
    tr['zh-tw_tl'] = "工具"
    tr['co_tl'] = "Strumenti"
    tr['hr_tl'] = "Alati"
    tr['cs_tl'] = "Nástroje"
    tr['da_tl'] = "Værktøjer"
    tr['nl_tl'] = "Hulpmiddelen"
    tr['en_tl'] = "Tools"
    tr['eo_tl'] = "Iloj"
    tr['et_tl'] = "Tööriistad"
    tr['tl_tl'] = "Mga gamit"
    tr['fi_tl'] = "Työkalut"
    tr['fr_tl'] = "Outils"
    tr['fy_tl'] = "Tools"
    tr['gl_tl'] = "Ferramentas"
    tr['ka_tl'] = "ხელსაწყოები"
    tr['de_tl'] = "Werkzeuge"
    tr['el_tl'] = "Εργαλεία"
    tr['gu_tl'] = "સાધનો"
    tr['ht_tl'] = "Zouti"
    tr['ha_tl'] = "Kayan aiki"
    tr['haw_tl'] = "Mea hana"
    tr['iw_tl'] = "כלים"
    tr['he_tl'] = "כלים"
    tr['hi_tl'] = "औजार"
    tr['hmn_tl'] = "Cov cuab yeej"
    tr['hu_tl'] = "Eszközök"
    tr['is_tl'] = "Verkfæri"
    tr['ig_tl'] = "Ngwa"
    tr['id_tl'] = "Peralatan"
    tr['ga_tl'] = "Uirlisí"
    tr['it_tl'] = "Utensili"
    tr['ja_tl'] = "ツール"
    tr['jw_tl'] = "piranti"
    tr['kn_tl'] = "ಪರಿಕರಗಳು"
    tr['kk_tl'] = "Құралдар"
    tr['km_tl'] = "ឧបករណ៍"
    tr['ko_tl'] = "도구"
    tr['ku_tl'] = "Amûrên"
    tr['ky_tl'] = "Куралдар"
    tr['lo_tl'] = "ເຄື່ອງມື"
    tr['la_tl'] = "Tools"
    tr['lv_tl'] = "Rīki"
    tr['lt_tl'] = "Įrankiai"
    tr['lb_tl'] = "Tools"
    tr['mk_tl'] = "Алатки"
    tr['mg_tl'] = "Tools"
    tr['ms_tl'] = "Alatan"
    tr['ml_tl'] = "ഉപകരണങ്ങൾ"
    tr['mt_tl'] = "Għodda"
    tr['mi_tl'] = "Utauta"
    tr['mr_tl'] = "साधने"
    tr['mn_tl'] = "Багаж хэрэгсэл"
    tr['my_tl'] = "ကိရိယာများ"
    tr['ne_tl'] = "उपकरणहरू"
    tr['no_tl'] = "Verktøy"
    tr['or_tl'] = "ସାଧନଗୁଡ଼ିକ |"
    tr['ps_tl'] = "وسیلې"
    tr['fa_tl'] = "ابزار"
    tr['pl_tl'] = "Narzędzia"
    tr['pt_tl'] = "Ferramentas"
    tr['pa_tl'] = "ਸੰਦ"
    tr['ro_tl'] = "Instrumente"
    tr['ru_tl'] = "Инструменты"
    tr['sm_tl'] = "Meafaigaluega"
    tr['gd_tl'] = "Innealan"
    tr['sr_tl'] = "Алати"
    tr['st_tl'] = "Lisebelisoa"
    tr['sn_tl'] = "Zvishandiso"
    tr['sd_tl'] = "اوزار"
    tr['si_tl'] = "මෙවලම්"
    tr['sk_tl'] = "Nástroje"
    tr['sl_tl'] = "Orodja"
    tr['so_tl'] = "Qalab"
    tr['es_tl'] = "Herramientas"
    tr['su_tl'] = "Parabot"
    tr['sw_tl'] = "Zana"
    tr['sv_tl'] = "Verktyg"
    tr['tg_tl'] = "Воситаҳо"
    tr['ta_tl'] = "கருவிகள்"
    tr['te_tl'] = "ఉపకరణాలు"
    tr['th_tl'] = "เครื่องมือ"
    tr['tr_tl'] = "Aletler"
    tr['uk_tl'] = "Інструменти"
    tr['ur_tl'] = "اوزار"
    tr['ug_tl'] = "قوراللار"
    tr['uz_tl'] = "Asboblar"
    tr['vi_tl'] = "Công cụ"
    tr['cy_tl'] = "Offer"
    tr['xh_tl'] = "Izixhobo"
    tr['yi_tl'] = "מכשירים"
    tr['yo_tl'] = "Awọn irinṣẹ"
    tr['zu_tl'] = "Amathuluzi"
    tr['af_li'] = "Skakels"
    tr['sq_li'] = "Lidhjet"
    tr['am_li'] = "አገናኞች"
    tr['ar_li'] = "روابط"
    tr['hy_li'] = "Հղումներ"
    tr['az_li'] = "Bağlantılar"
    tr['eu_li'] = "Estekak"
    tr['be_li'] = "Спасылкі"
    tr['bn_li'] = "লিঙ্ক"
    tr['bs_li'] = "Linkovi"
    tr['bg_li'] = "Връзки"
    tr['ca_li'] = "Enllaços"
    tr['ceb_li'] = "Mga link"
    tr['ny_li'] = "Maulalo"
    tr['zh-cn_li'] = "链接"
    tr['zh-tw_li'] = "連結"
    tr['co_li'] = "Ligami"
    tr['hr_li'] = "Linkovi"
    tr['cs_li'] = "Odkazy"
    tr['da_li'] = "Links"
    tr['nl_li'] = "Koppelingen"
    tr['en_li'] = "Links"
    tr['eo_li'] = "Ligiloj"
    tr['et_li'] = "Lingid"
    tr['tl_li'] = "Mga link"
    tr['fi_li'] = "Linkit"
    tr['fr_li'] = "Liens"
    tr['fy_li'] = "Links"
    tr['gl_li'] = "Ligazóns"
    tr['ka_li'] = "ბმულები"
    tr['de_li'] = "Links"
    tr['el_li'] = "Συνδέσεις"
    tr['gu_li'] = "લિંક્સ"
    tr['ht_li'] = "Lyen"
    tr['ha_li'] = "Hanyoyin haɗi"
    tr['haw_li'] = "Nā loulou"
    tr['iw_li'] = "קישורים"
    tr['he_li'] = "קישורים"
    tr['hi_li'] = "लिंक"
    tr['hmn_li'] = "Txuas"
    tr['hu_li'] = "Linkek"
    tr['is_li'] = "Tenglar"
    tr['ig_li'] = "Njikọ"
    tr['id_li'] = "Tautan"
    tr['ga_li'] = "Naisc"
    tr['it_li'] = "Collegamenti"
    tr['ja_li'] = "リンク"
    tr['jw_li'] = "Pranala"
    tr['kn_li'] = "ಲಿಂಕ್‌ಗಳು"
    tr['kk_li'] = "Сілтемелер"
    tr['km_li'] = "តំណភ្ជាប់"
    tr['ko_li'] = "연결"
    tr['ku_li'] = "Girêdanên"
    tr['ky_li'] = "Шилтемелер"
    tr['lo_li'] = "ລິ້ງຄ໌"
    tr['la_li'] = "Vincula"
    tr['lv_li'] = "Saites"
    tr['lt_li'] = "Nuorodos"
    tr['lb_li'] = "Linken"
    tr['mk_li'] = "Врски"
    tr['mg_li'] = "rohy"
    tr['ms_li'] = "Pautan"
    tr['ml_li'] = "ലിങ്കുകൾ"
    tr['mt_li'] = "Links"
    tr['mi_li'] = "Hononga"
    tr['mr_li'] = "दुवे"
    tr['mn_li'] = "Холбоосууд"
    tr['my_li'] = "လင့်များ"
    tr['ne_li'] = "लिङ्कहरू"
    tr['no_li'] = "Lenker"
    tr['or_li'] = "ଲିଙ୍କ୍"
    tr['ps_li'] = "لینکونه"
    tr['fa_li'] = "پیوندها"
    tr['pl_li'] = "Spinki do mankietów"
    tr['pt_li'] = "Ligações"
    tr['pa_li'] = "ਲਿੰਕ"
    tr['ro_li'] = "Legături"
    tr['ru_li'] = "Ссылки"
    tr['sm_li'] = "So'oga"
    tr['gd_li'] = "Ceanglaichean"
    tr['sr_li'] = "Линкови"
    tr['st_li'] = "Lihokelo"
    tr['sn_li'] = "Links"
    tr['sd_li'] = "لنڪس"
    tr['si_li'] = "සබැඳි"
    tr['sk_li'] = "Odkazy"
    tr['sl_li'] = "Povezave"
    tr['so_li'] = "Xiriirinta"
    tr['es_li'] = "Enlaces"
    tr['su_li'] = "Tumbu"
    tr['sw_li'] = "Viungo"
    tr['sv_li'] = "Länkar"
    tr['tg_li'] = "Пайвандҳо"
    tr['ta_li'] = "இணைப்புகள்"
    tr['te_li'] = "లింకులు"
    tr['th_li'] = "ลิงค์"
    tr['tr_li'] = "Bağlantılar"
    tr['uk_li'] = "Посилання"
    tr['ur_li'] = "لنکس"
    tr['ug_li'] = "ئۇلىنىشلار"
    tr['uz_li'] = "Havolalar"
    tr['vi_li'] = "Liên kết"
    tr['cy_li'] = "Cysylltiadau"
    tr['xh_li'] = "Unxulumano"
    tr['yi_li'] = "לינקס"
    tr['yo_li'] = "Awọn ọna asopọ"
    tr['zu_li'] = "Izixhumanisi"
    tr['af_dt'] = "Datastelle"
    tr['sq_dt'] = "Grupet e të dhënave"
    tr['am_dt'] = "የውሂብ ስብስቦች"
    tr['ar_dt'] = "مجموعات البيانات"
    tr['hy_dt'] = "Տվյալների հավաքածուներ"
    tr['az_dt'] = "Məlumat dəstləri"
    tr['eu_dt'] = "Datu multzoak"
    tr['be_dt'] = "Наборы дадзеных"
    tr['bn_dt'] = "ডেটাসেট"
    tr['bs_dt'] = "Skupovi podataka"
    tr['bg_dt'] = "Набори от данни"
    tr['ca_dt'] = "Conjunts de dades"
    tr['ceb_dt'] = "Mga Dataset"
    tr['ny_dt'] = "Ma datasets"
    tr['zh-cn_dt'] = "数据集"
    tr['zh-tw_dt'] = "數據集"
    tr['co_dt'] = "Datasets"
    tr['hr_dt'] = "Skupovi podataka"
    tr['cs_dt'] = "Datové sady"
    tr['da_dt'] = "Datasæt"
    tr['nl_dt'] = "Gegevenssets"
    tr['en_dt'] = "Datasets"
    tr['eo_dt'] = "Datumaroj"
    tr['et_dt'] = "Andmekogumid"
    tr['tl_dt'] = "Mga Dataset"
    tr['fi_dt'] = "Tietojoukot"
    tr['fr_dt'] = "Ensembles de données"
    tr['fy_dt'] = "Datasets"
    tr['gl_dt'] = "Conxuntos de datos"
    tr['ka_dt'] = "მონაცემთა ნაკრები"
    tr['de_dt'] = "Datensätze"
    tr['el_dt'] = "Σύνολα δεδομένων"
    tr['gu_dt'] = "ડેટાસેટ્સ"
    tr['ht_dt'] = "Ansanm done"
    tr['ha_dt'] = "Bayanan bayanai"
    tr['haw_dt'] = "Nā waihona ʻikepili"
    tr['iw_dt'] = "מערכי נתונים"
    tr['he_dt'] = "מערכי נתונים"
    tr['hi_dt'] = "डेटासेट"
    tr['hmn_dt'] = "Cov ntaub ntawv"
    tr['hu_dt'] = "Adatkészletek"
    tr['is_dt'] = "Gagnasett"
    tr['ig_dt'] = "Ntọala data"
    tr['id_dt'] = "Kumpulan data"
    tr['ga_dt'] = "tacair sonraí"
    tr['it_dt'] = "Set di dati"
    tr['ja_dt'] = "データセット"
    tr['jw_dt'] = "kumpulan data"
    tr['kn_dt'] = "ಡೇಟಾಸೆಟ್‌ಗಳು"
    tr['kk_dt'] = "Деректер жиындары"
    tr['km_dt'] = "សំណុំទិន្នន័យ"
    tr['ko_dt'] = "데이터세트"
    tr['ku_dt'] = "Datasets"
    tr['ky_dt'] = "Берилиштер топтому"
    tr['lo_dt'] = "ຊຸດຂໍ້ມູນ"
    tr['la_dt'] = "Datasets"
    tr['lv_dt'] = "Datu kopas"
    tr['lt_dt'] = "Duomenų rinkiniai"
    tr['lb_dt'] = "Datesets"
    tr['mk_dt'] = "Збирки на податоци"
    tr['mg_dt'] = "Datasets"
    tr['ms_dt'] = "Set data"
    tr['ml_dt'] = "ഡാറ്റാസെറ്റുകൾ"
    tr['mt_dt'] = "Settijiet tad-dejta"
    tr['mi_dt'] = "Raraunga Raraunga"
    tr['mr_dt'] = "डेटासेट"
    tr['mn_dt'] = "Өгөгдлийн багц"
    tr['my_dt'] = "ဒေတာအစုံများ"
    tr['ne_dt'] = "डाटासेटहरू"
    tr['no_dt'] = "Datasett"
    tr['or_dt'] = "ଡାଟାବେସ୍"
    tr['ps_dt'] = "ډیټاسیټونه"
    tr['fa_dt'] = "مجموعه داده ها"
    tr['pl_dt'] = "Zbiory danych"
    tr['pt_dt'] = "Conjuntos de dados"
    tr['pa_dt'] = "ਡਾਟਾਸੈੱਟ"
    tr['ro_dt'] = "Seturi de date"
    tr['ru_dt'] = "Наборы данных"
    tr['sm_dt'] = "Fa'amaumauga"
    tr['gd_dt'] = "Stòr-dàta"
    tr['sr_dt'] = "Скупови података"
    tr['st_dt'] = "Lisebelisoa tsa data"
    tr['sn_dt'] = "Datasets"
    tr['sd_dt'] = "ڊيٽا سيٽ"
    tr['si_dt'] = "දත්ත කාණ්ඩ"
    tr['sk_dt'] = "Množiny údajov"
    tr['sl_dt'] = "Nabori podatkov"
    tr['so_dt'] = "Xog ururinta"
    tr['es_dt'] = "Conjuntos de datos"
    tr['su_dt'] = "Datasets"
    tr['sw_dt'] = "Seti za data"
    tr['sv_dt'] = "Datauppsättningar"
    tr['tg_dt'] = "Маҷмӯи додаҳо"
    tr['ta_dt'] = "தரவுத்தொகுப்புகள்"
    tr['te_dt'] = "డేటాసెట్‌లు"
    tr['th_dt'] = "ชุดข้อมูล"
    tr['tr_dt'] = "Veri kümeleri"
    tr['uk_dt'] = "Набори даних"
    tr['ur_dt'] = "ڈیٹاسیٹس"
    tr['ug_dt'] = "Datasets"
    tr['uz_dt'] = "Ma'lumotlar to'plami"
    tr['vi_dt'] = "Bộ dữ liệu"
    tr['cy_dt'] = "Setiau data"
    tr['xh_dt'] = "Iiseti zedatha"
    tr['yi_dt'] = "דאַטאַסעטס"
    tr['yo_dt'] = "Awọn ipilẹ data"
    tr['zu_dt'] = "Amasethi edatha"

dat_apps_dir = APPS_PATH + "/Data Science Apps/"
dat_extr_dir = APPS_PATH + "/DAT Linux Extras/"
dat_admn_dir = APPS_PATH + "/DAT Linux Admin/"
dat_pro_dir = APPS_PATH + "/DAT Linux Pro/"
dat_help_dir = APPS_PATH + "/DAT Linux Help/"
pro_tools_dir = '/opt/datlinux/pro/tools/'
pro_conf_file=str(Path.home()) + '/.config/datlinux/pro/pro.conf'
pro_links_dir = str(Path.home()) + '/.local/share/applications/datlinux/links/'

loc_ctrpnl = 'Control Panel'
loc_about = 'About'
loc_distr = 'Distribution'
if lang != 'en':
    loc_ctrpnl = tr[f"{lang}_cp"]
    loc_about = tr[f"{lang}_ab"]
    loc_distr = tr[f"{lang}_di"]
root = ThemedTk(theme="clearlooks", className=f"DAT Linux {loc_ctrpnl}")
root.title(f"DAT Linux {loc_ctrpnl}")
ico = tk.PhotoImage(file='/usr/share/icons/datlinux/datlinux_logo.png')
root.iconphoto(True, ico)
root.geometry("962x620")
root.resizable(0, 0)

style = ttk.Style()

def get_theme():
  if os.path.isfile(pro_tools_dir + 'Monitor.datlinux.desktop') and os.path.isfile(pro_conf_file):
      with open(pro_conf_file) as file:
          for line in file:
              l = line.rstrip()
              if l.startswith('PRO-THEME'):
                  return l.split('=')[1]
  return "light"

theme = get_theme() # On load.

def toggle_theme():
    global theme
    if theme == "light":
        style.configure("TNotebook", foreground="#050505", background="#EFEBE7")
        style.configure('TNotebook.Tab', foreground="#000000", background="#C9C1BC")
        style.map("TNotebook.Tab", background=[("selected", "#EFEBE7")])   
    if theme == "dark":
        style.configure("TNotebook", foreground="#EFEBE7", background="#555d50", borderwidth=0)
        style.configure('TNotebook.Tab', foreground="#EFEBE7", background="#71797E")
        style.map("TNotebook.Tab", background=[("selected", "#555d50")])  

toggle_theme() # On load.

def set_theme():
    ret = False
    global theme
    check_theme = get_theme()
    if theme != check_theme:
        ret = True
        theme = check_theme
        print('Theme change: ' + theme)
        toggle_theme()
    return ret # Has changed.

def set_button_theme(btn):
    if theme == "light":
        btn['bg'] = '#FBFBFB'
        btn['fg'] = '#050505'
        btn['activeforeground'] = '#555d50'
        btn['activebackground'] = '#FBFBFB'        
    else:
        btn['bg'] = '#2E3436'
        btn['fg'] = '#FBFBFB'
        btn['activeforeground'] = '#FBFBFB'
        btn['activebackground'] = '#555d50'
        
def set_button_off_theme(btn):
    if theme == "light":
        btn['bg'] = '#EFEBE7'
        btn['fg'] = '#2e3436'
        btn['activeforeground'] = '#555d50'
        btn['activebackground'] = '#FBFBFB'        
    else:
        btn['bg'] = '#555d50'
        btn['fg'] = '#FBFBFB'
        btn['activeforeground'] = '#FBFBFB'
        btn['activebackground'] = '#555d50'

tabControl = ttk.Notebook(root)
tab1 = ttk.Frame(tabControl, style='TNotebook')
tab2 = ttk.Frame(tabControl, style='TNotebook')
tab3 = ttk.Frame(tabControl, style='TNotebook')
tab4 = ttk.Frame(tabControl, style='TNotebook')
tab5 = ttk.Frame(tabControl, style='TNotebook')
tab6 = ttk.Frame(tabControl, style='TNotebook')

loc_vars = {'ds_1' : "Data Science",
            'ds_2' : "Apps",
            'ex' : "Extras",
            'ad' : "Admin",
            'he' : "Help",
            'tl' : "Tools",
            'li' : "Links",
            'dt' : "Datasets"}

if lang != 'en':
    loc_vars['ds_1'] = tr[f"{lang}_ds_1"]
    loc_vars['ds_2'] = tr[f"{lang}_ds_2"]
    loc_vars['ex'] = tr[f"{lang}_ex"]
    loc_vars['ad'] = tr[f"{lang}_ad"]
    loc_vars['he'] = tr[f"{lang}_he"]
    loc_vars['tl'] = tr[f"{lang}_tl"]
    loc_vars['li'] = tr[f"{lang}_li"]
    loc_vars['dt'] = tr[f"{lang}_dt"]

tabControl.add(tab1, text=f" {loc_vars['ds_1']}   \n {loc_vars['ds_2']} ")
tabControl.add(tab2, text=f" {loc_vars['ds_1']}   \n {loc_vars['ex']} ")
tabControl.add(tab3, text=f" {loc_vars['ad']}   \n ")

if os.path.isfile(pro_tools_dir + 'Monitor.datlinux.desktop'):
    tabControl.add(tab4, text=f" 🧰️ Pro\n     {loc_vars['tl']} ")
else:
    tabControl.add(tab4, text=" 🧰️ Pro\n")
if os.path.isdir(pro_links_dir):
    tabControl.add(tab5, text=" Links\n") 
tabControl.add(tab6, text=f" {loc_vars['he']}    \n    ")

tabControl.pack(expand=1, fill="both")
tabControl.grid(column = 1,
               row = 1,
               padx = 0,
               pady = 0,
               sticky=tk.N)
vers=os.popen('apt show datlinux-ctrl-pnl | grep "Version"').read()
barter = tk.Label(root, text=f"{loc_ctrpnl} " + vers,
                  font=('"Ubuntu" 9 normal'), justify=tk.LEFT)
barter.grid(column = 1,
               row = 2,
               padx = 3,
               pady = 4,
               sticky=tk.W)

myFont = font.Font(family='Ubuntu', size=9, weight="normal")

def status_msg(msg):
    barter['text'] = msg
    time.sleep(3)
    barter['text'] = f"{loc_ctrpnl} " + vers
  
def run_cmd(cmd):
    os.system("nohup " + cmd + " &>/dev/null & sleep 1")

def btn_clicked(cmd, nm):
    x = threading.Thread(target=status_msg,
                         args=('⚙️ ' + nm + '..',))
    x.start()
    y = threading.Thread(target=run_cmd, args=(cmd,))
    y.start()

def about_btn_clicked():
    x = threading.Thread(target=status_msg,
                         args=(f"⚙️ {loc_about}..",))
    x.start()
    y = threading.Thread(target=about_popup)
    y.start()

def truncate_name(n):
    if len(n) > 10:
        return n[:9] + '-\n' + n[9:20]
    return n + '\n'

apps_btns_dict = {}

def create_button(name_, exec_, icon_, desc_, tab, reg_bg=True, app=''):
    spl = name_.split(' ')
    if len(spl) > 1:
        namex_ = spl[0][:10] + '\n' + spl[1][0:10]
    else:
        namex_ = truncate_name(name_)
    
    if app != '':
        k = namex_.replace("\n", "")
        apps_btns_dict[k] = app

    img = Image.open('/usr/share/icons/datlinux/datlinux.png') # failsafe
    try:
        img = Image.open(icon_)
    except:
        print('oops.. ImageTk.PhotoImage failed!')
    img = img.resize((60, 60))
    img = ImageTk.PhotoImage(img)
    if name_ == f"{loc_about}":
        action_with_arg = partial(about_btn_clicked)
    else:
        action_with_arg = partial(btn_clicked, exec_, name_)

    butt = tk.Button(
        tab,
        cursor="hand1",
        bd=1,
        width=66,
        highlightthickness=0,
        image=img,
        text=namex_,
        justify=tk.CENTER,
        compound=tk.TOP,
        command=action_with_arg
    )
    if reg_bg == False:
        set_button_off_theme(butt)
    else:
        set_button_theme(butt)

    butt.image = img
    butt['font'] = myFont
    Hovertip(butt, textwrap.fill(desc_, 52))
    return butt

def create_tab_buttons(apps, dirr, tab, cols, is_apps=False):
    cari = None
    if is_apps == True:
        cari = os.popen('cari list | cut -d" " -f1').read().split("\n")
    btns = []
    col = 0
    row = 0
    app = ''
    lanng = system_lang()
    apps.sort(key=str.lower)
    for f in apps:
        app = ''
        reg_bg = True
        if f.endswith('.pdf'):
            spl = f.split('/')
            name_ = spl[len(spl)-1]
            name_ = name_.replace("DAT Linux FAQ", "DAT-Linux FAQ").replace(".pdf","")
            exec_ = 'xdg-open "' + dirr + f + '"'
            icon_ = '/usr/share/icons/datlinux/pdf.png'
            desc_ = name_
        elif not f.endswith('.desktop'):
            continue
        else:
            config = configparser.RawConfigParser()
            config.read(dirr + f)
            name_ = config.get("Desktop Entry", "Name")
            exec_ = config.get("Desktop Entry", "Exec")
            exec_ = re.sub(' %.', '', exec_)
            icon_ = config.get("Desktop Entry", "Icon")
            desc_ = config.get("Desktop Entry", "Comment")
            try:
                if lanng != "en":
                    desc_ = config.get("Desktop Entry", f"Comment[{lanng}]")
            except:
                print(f'No {lanng}!')
                print(dirr + f)
            
            if is_apps == True:
                app = config.get("Desktop Entry", "App")
                reg_bg = False
                for c in cari:
                    if c.startswith(app):
                        reg_bg = True
                        break
        butt = create_button(name_, exec_, icon_, desc_, tab, reg_bg, app)
        butt.grid(column = col,
                  row = row+1,
                  padx = 2,
                  pady = 2,
                  sticky=tk.N)

        if col == cols-1:
            col = 0
            row = row + 2
        else:
            col = col + 1

        btns.append(butt)

    if dirr == dat_help_dir:
        btn = create_button(f"{loc_about}", '', '/usr/share/icons/datlinux/about.png',
                            f"About DAT Linux {loc_ctrpnl} {CTRLPNLVER}", tab, True)
        btn.grid(column = col,
                 row = row+1,
                 padx = 2,
                 pady = 2,
                 sticky=tk.N)
        btns.append(btn)
    
    return btns

def get_configs(dirr):
    ret = []
    files = os.listdir(dirr)
    for f in files:
        ret.append(f)
    return ret

def webbrowser_callback():
    webbrowser.open_new(f"{REPO}/-/raw/main/README.md")
    
def about_popup():
    top = tk.Toplevel(root)
    top.geometry("730x406")
    top.title(f"{loc_about}")
    msg1_1=os.popen('echo $XDG_CURRENT_DESKTOP').read()
    msg2_1=os.popen('uname -r').read()
    msg3=f"⚙️ DAT Linux {loc_ctrpnl}:"
    msg4=os.popen('apt show datlinux-ctrl-pnl').read()
    msg=f"""Kernel:         {msg2_1}Desktop:        {msg1_1}
{msg3}

{msg4}"""
    tk.Label(top, text=f"💿️ {loc_distr}:", font=('"Ubuntu Mono" 11'),
            justify=tk.LEFT).place(x=8,y=8)
    tk.Label(top, text="DAT Linux 1.0.x-lxqt",
            justify=tk.LEFT).place(x=8,y=32)
    tk.Label(top, text=msg, font=('"Ubuntu Mono" 11'),
            justify=tk.LEFT).place(x=8,y=65)
    tk.Button(
        top,
        fg="dark blue",
        cursor="hand1",
        bd=1,
        width=12,
        highlightthickness=0,
        text="📄️ Release notes",
        justify=tk.CENTER,
        command=webbrowser_callback
    ).place(x=568,y=6)

apps_buttons = []
extras_buttons = create_tab_buttons(get_configs(dat_extr_dir), dat_extr_dir, tab2, 10)
admin_buttons = create_tab_buttons(get_configs(dat_admn_dir), dat_admn_dir, tab3, 10)
is_pro = False
pro_buttons = []
if os.path.isfile(pro_tools_dir + 'Monitor.datlinux.desktop'):
    is_pro = True
    pro_buttons = create_tab_buttons(get_configs(pro_tools_dir), pro_tools_dir, tab4, 10)
else:
    pro_buttons = create_tab_buttons(get_configs(dat_pro_dir), dat_pro_dir, tab4, 10)
help_buttons = create_tab_buttons(get_configs(dat_help_dir), dat_help_dir, tab6, 10)
links_buttons = []
if os.path.isdir(pro_links_dir):
    links_buttons = create_tab_buttons(get_configs(pro_links_dir), pro_links_dir, tab5, 10)

def display_apps_btns():
    cari = os.popen('cari list | cut -d" " -f1').read().split("\n")
    for btn in apps_buttons:
        app_found = False
        k = ''
        for c in cari:
            k = str(btn['text']).replace("\n", "")
            if c.startswith(apps_btns_dict[k]):
                app_found = True
                break
        if app_found == False:
            set_button_off_theme(btn)
        else:
            set_button_theme(btn)

class AppsThread(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self.stopped = event

    def run(self):
        global apps_buttons
        apps_buttons = create_tab_buttons(get_configs(dat_apps_dir), dat_apps_dir, tab1, 10, True)
        while not self.stopped.wait(15):
            display_apps_btns()

stopFlag = Event()
thread = AppsThread(stopFlag)
thread.start()
if os.path.isdir(pro_links_dir):
    pro_links_dir_mod_time = os.path.getmtime(pro_links_dir)
class ThemeLinksThread(Thread):
    def __init__(self, event):
        Thread.__init__(self)
        self.stopped = event

    def run(self):
        global apps_buttons
        global extras_buttons
        global admin_buttons
        global pro_buttons
        global links_buttons
        global help_buttons
        global pro_links_dir_mod_time
        while not self.stopped.wait(3):
            if os.path.isdir(pro_links_dir):
                tm = os.path.getmtime(pro_links_dir)
                if pro_links_dir_mod_time == tm:
                    print('No change to pro_links_dir_mod_time..')
                else:
                    print('Change to pro_links_dir_mod_time!')
                    pro_links_dir_mod_time = tm
                    for widget in tab5.winfo_children():
                        widget.destroy()
                    links_buttons = create_tab_buttons(get_configs(pro_links_dir), pro_links_dir, tab5, 10)        
            if set_theme():
                display_apps_btns()
                for b in extras_buttons:
                    set_button_theme(b)
                for b in admin_buttons:
                    set_button_theme(b)
                for b in pro_buttons:
                    set_button_theme(b)
                for b in links_buttons:
                    set_button_theme(b)
                for b in help_buttons:
                    set_button_theme(b)

thread2 = ThemeLinksThread(stopFlag)
thread2.start()

def stop(): # stop button to close the gui and should terminate the download function too
    print('stopping')
    try:
        get_lock._lock_socket.close()
    except socket.error:
        None
    stopFlag.set()
    root.destroy()

# Callback to check app or pro updates daily:
tod = datetime.today().strftime('%Y-%m-%d')
f_tod = str(Path.home()) + "/.cache/datlinux/updchk-" + tod
if not os.path.isfile(f_tod):
    os.system('/opt/datlinux/pro/api pro_notify &')
    os.system('/opt/datlinux/pro/api apps_notify &')
    os.system('rm -f ' + str(Path.home()) + '/.cache/datlinux/updchk-* && touch ' + f_tod + ' &')

root.protocol('WM_DELETE_WINDOW', stop)
   
root.mainloop()
