#!/bin/sh

REL="1.0.171-lxqt"
DISTR="1.0.x-lxqt"
DATLINUXVER="\${exec \/bin\/bash -c '\. \/etc\/environment; if [ -z \${DATLINUX_RELEASE+x} ]; then echo \"1.0.x\"; else echo \"\$DATLINUX_RELEASE\"; fi'}"

wget -O "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_XXXXX_amd64/usr/share/applications/datlinux/DAT Linux Help/DAT Linux FAQ.pdf" \
"https://datlinux.gitlab.io/DAT%20Linux%201.0.x%20FAQ.pdf"
wget -O "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_XXXXX_amd64/usr/share/applications/datlinux/DAT Linux Help/About Apps.pdf" \
"https://datlinux.gitlab.io/About%20Apps.pdf"

find $HOME/Gitlab/datlinux-control-panel-1.0.x-lxqt/datlinux_XXXXX_amd64/usr/local/etc/dconky -type f -exec sed -i "s/DAT Linux .*/DAT Linux ${DATLINUXVER}/g" {} \;

sed -i "s/Version: .*/Version: ${REL}/g" "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_XXXXX_amd64/DEBIAN/control"
sed -i "s/PRETTY_NAME.*/PRETTY_NAME=\"DAT Linux ${DATLINUXVER}\" >> \/etc\/os-release'/g" "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_XXXXX_amd64/DEBIAN/postinst"
sed -i "s/text=\"DAT Linux .*/text=\"DAT Linux ${DISTR}\",/g" "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_XXXXX_amd64/usr/local/bin/datlinux/dat-dsapps.py"
sed -i "s/CTRLPNLVER=.*/CTRLPNLVER='${REL}'/g" "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_XXXXX_amd64/usr/local/bin/datlinux/dat-dsapps.py"

rm $HOME/Gitlab/datlinux-control-panel-$DISTR/*.deb

mkdir "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_${REL}_amd64"
cp -r $HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_XXXXX_amd64/* "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_${REL}_amd64"

cd "$HOME/Gitlab/datlinux-control-panel-$DISTR"
dpkg-deb --build datlinux_${REL}_amd64/
rm -rf ./apt-repo/pool/main/
mkdir -p ./apt-repo/pool/main/
cp datlinux_${REL}_amd64.deb ./apt-repo/pool/main/
mkdir -p ./apt-repo/dists/stable/main/binary-amd64
cd ./apt-repo/
dpkg-scanpackages --arch amd64 pool/ > dists/stable/main/binary-amd64/Packages
cat dists/stable/main/binary-amd64/Packages | gzip -9 > dists/stable/main/binary-amd64/Packages.gz
cd "$HOME/Gitlab/datlinux-control-panel-$DISTR"

echo '#!/bin/sh
set -e
do_hash() {
    HASH_NAME=$1
    HASH_CMD=$2
    echo "${HASH_NAME}:"
    for f in $(find -type f); do
        f=$(echo $f | cut -c3-) # remove ./ prefix
        if [ "$f" = "Release" ]; then
            continue
        fi
        echo " $(${HASH_CMD} ${f}  | cut -d" " -f1) $(wc -c $f)"
    done
}
cat << EOF
Origin: DAT Linux Repository
Label: DAT Linux
Suite: stable
Codename: stable' > ./apt-repo/dists/stable/generate-release.sh &&
echo "Version: ${REL}" >> ./apt-repo/dists/stable/generate-release.sh &&
echo 'Architectures: amd64
Components: main
Description: DAT Linux Repository
Date: $(date -Ru)
EOF
do_hash "MD5Sum" "md5sum"
do_hash "SHA1" "sha1sum"
do_hash "SHA256" "sha256sum"
' >> ./apt-repo/dists/stable/generate-release.sh && chmod +x ./apt-repo/dists/stable/generate-release.sh

cd ./apt-repo/dists/stable
./generate-release.sh > Release
rm generate-release.sh

cd "$HOME/Gitlab/datlinux-control-panel-$DISTR"

#sudo echo "%echo DAT Linux PGP key
#Key-Type: RSA
#Key-Length: 4096
#Name-Real: datlinux
#Name-Email: info@datlinux.com
#Expire-Date: 0
#%no-ask-passphrase
#%no-protection
#%commit" > /tmp/datlinux-pgp-key.batch

export GNUPGHOME="./pgpkeys-Puqya8"

#gpg --no-tty --batch --gen-key /tmp/datlinux-pgp-key.batch
#gpg --armor --export datlinux > pgp-key.public
#gpg --armor --export-secret-keys datlinux > pgp-key.private
#cat pgp-key.private | gpg --import

cat ./apt-repo/dists/stable/Release | gpg --default-key datlinux -abs > ./apt-repo/dists/stable/Release.gpg
cat ./apt-repo/dists/stable/Release | gpg --default-key datlinux -abs --clearsign > ./apt-repo/dists/stable/InRelease

cp pgp-key.public ./apt-repo/
rm -rf "$HOME/Gitlab/datlinux-control-panel-$DISTR/datlinux_${REL}_amd64"

# Push up apt-repo to remote:
# git add -A
# git commit -a -m "latest"
# git push origin main
