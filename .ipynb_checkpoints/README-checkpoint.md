# DAT Linux Control Panel

--------------------------------------------------------------------------------

## Release notes for the `DAT Linux 1.0.x-lxqt` edition of the control panel

-------------------------------------------------------------------------------- 

### Version "1.0.171-lxqt", release date 12 Nov 2024

- House cleaning updates

### Version "1.0.170-lxqt", release date 18 Oct 2024

- Fix bug for IJulia jupyter kernel install/uninstall

### Version "1.0.169-lxqt", release date 18 Oct 2024

- Fix bug for sysinfo logo

### Version "1.0.166-lxqt", release date 15 Oct 2024

- Fix csvkit drop into virtual env

### Version "1.0.163-lxqt", release date 10 May 2024

- Fix bug when killing Superset

### Version "1.0.162-lxqt", release date 28 Mar 2024

- Check for Java 17 on DBeaver install

### Version "1.0.161-lxqt", release date 27 Mar 2024

- Added "Ebooks Search" to extras tab
- Translations for PRO icons

### Version "1.0.160-lxqt", release date 20 Feb 2024

- Minor typo fixes

### Version "1.0.159-lxqt", release date 18 Feb 2024

- Check for $DATLINUX_RELEASE global in dconky

### Version "1.0.157-lxqt", release date 13 Feb 2024

- csvkit app migrated to pip installer

### Version "1.0.155-lxqt", release date 11 Feb 2024

- Fix bug with pip pkg transfers
- Fixed cari reshim so that it only shims the app executable
- DAT Linux PRO launch!

### Version "1.0.154-lxqt", release date 04 Feb 2024

- Fix bug with birt uninstaller
- Improve internet check
- Refactor [app]-setup scripts

### Version "1.0.153-lxqt", release date 28 Jan 2024

- No longer using cache for ctrl panel icons
- Better ping
- DAT Linux PRO installer & tools first launch

### Version "1.0.152-lxqt", release date 06 Jan 2024

- Added Grafana app & launcher

### Version "1.0.148-lxqt", release date O6 Oct 2023

- FAQ updated for DAT Linux 1.0.2 

### Version "1.0.143-lxqt", release date 20 May 2023

- Tweak to Superset setup check if 3.9.13 installed

### Version "1.0.142-lxqt", release date 18 May 2023

- Patch for IJulia updates (IJulia installed/uninstalled/updated with Julia)

### Version "1.0.141-lxqt", release date 20 Mar 2023

- Bug fix for RStudio app launcher script

### Version "1.0.140-lxqt", release date 07 Mar 2023

- Added Gephi app & launcher
- Control panel widened for extra apps

### Version "1.0.139-lxqt", release date 26 Feb 2023

- Tooltip fixes on Help (PDF) icons & new icon logo
- Ctrl panel icons cache now release-sensitive
- Help/About tooltip shows ctrl pnl version
- Extras/Quarto icon logo fix

### Version "1.0.138-lxqt", release date 23 Feb 2023

- Bug fix Quarto app launcher - display version correctly
- "Launch again" instead of auto launch on Emacs+ESS install

### Version "1.0.137-lxqt", release date 20 Feb 2023

- Emergency patch to the Quarto app installer

### Version "1.0.136-lxqt", release date 20 Feb 2023

- Added Quarto installer to 'Extras' tab

### Version "1.0.135-lxqt", release date 16 Feb 2023

- Updated "About Apps"

### Version "1.0.134-lxqt", release date 15 Feb 2023

- Added Emacs+ESS app & launcher
- Added ESS manual in "extras"

### Version "1.0.133-lxqt", release date 06 Feb 2023

- Redundant "free books" link removed
- Updated some "Help" icons

### Version "1.0.132-lxqt", release date 02 Jan 2023

- Added Scilab app & launcher

### Version "1.0.131-lxqt", release date 15 Dec 2022

- CSVkit promoted to first class DAT Linux app

### Version "1.0.130-lxqt", release date 03 Dec 2022

- Bug fix of background check that removes duplicate app icons
- Bug fix to ensure .cari/utils.bash copied incorrectly
- Delete .cache/datlinux icons on ctrl panel upgrade
- Lots of refactoring to move operations/files out of home dirs
- Show maintainer in About popup
- Minor typo fix in Spyder setup
- System info "conky" set to no window decorations
- New dconky/sysinfo title
- Added "on|off" to "conky" tooltip
- Dropped extraneous translations in desktop icons
- Re-categorised menu items more sensibly

### Version "1.0.86-lxqt", release date 26 Nov 2022

- Added Apache Druid app & launcher
- Added DuckDB app & launcher

### Version "1.0.85-lxqt", release date 20 Nov 2022

- Improved logic and timing control when starting servers and opening apps for
Superset, Metabase, Zeppelin launchers
- Tweaked the Metabase installer: now opens the options menu after install
instead of opening/running the server/app
- Tweaked the Zeppelin installer: now opens the options menu after install
instead of opening/running the server/app
- Tweaked the Superset installer: now opens the options menu after install
instead of opening/running the server/app
- Link in the "About" launcher on the 'Help' tab to open these release notes.
